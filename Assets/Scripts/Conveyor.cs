﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Conveyor : MonoBehaviour
{
    public GameObject prefab;
    public GameObject target;
    public float speed;
    public float spawnInterval;

    public List<GameObject> list;

    private float _spawnTimer;
    
	private void Start()
    {
        list = new List<GameObject>();
	}
	
	void Update()
    {
        _spawnTimer += Time.deltaTime;
        if (_spawnTimer > spawnInterval)
        {
            _spawnTimer = 0;
            GameObject item = GameObject.Instantiate(prefab, transform);
            list.Add(item);
        }

        for (int i = 0; i < list.Count; i++)
        {
            list[i].transform.position = Vector3.MoveTowards(
                list[i].transform.position,
                target.transform.position,
                speed * Time.deltaTime
            );

            if (Vector3.Distance(list[i].transform.position, target.transform.position) < float.Epsilon)
            {
                Destroy(list[i]);
                list.RemoveAt(i);
            }
        }
    }
}
