﻿using System;
using UnityEngine;

public class PortAllIPFilterMission : Mission
{

    public PortAllIPFilterMission(int port, string originIP, string destinationIP)
    {
        this.port = port;
        this.originIP = originIP;
        this.destinationIP = destinationIP;
    }

    public override string Description
    {
        get { return String.Format("Discard malicious packets\nFrom the rest, reject the ones with port {0}, origin IP {1} or destination IP {2}", port, originIP, destinationIP); }
    }

    public override int Difficulty
    {
        get { return 4; }
    }

    public override bool IsLegalMove(Packet packet, GameObject output)
    {
        Output belt = output.GetComponent<Output>();
        bool sentToTrash = (belt == null);

        if (!sentToTrash)
        {
            if (packet.type == PacketType.HACKER || packet.port == port || packet.ipDestination == destinationIP || packet.ipOrigin == originIP)
            {
                return false;
            }
            else
            {
                if (belt.load >= GameController.instance.maxOutputLoad)
                {
                    return false;
                }
            }
        }
        else
        {
            if (packet.type == PacketType.GOOD && packet.port != port && packet.ipDestination != destinationIP && packet.ipOrigin != originIP)
            {
                return false;
            }
        }
        return true;
    }
}
