﻿using System;
using UnityEngine;

public class PortDestinationIPFilterMission : Mission
{

    public PortDestinationIPFilterMission(int port, string destinationIP)
    {
        this.port = port;
        this.destinationIP = destinationIP;
    }

    public override string Description
    {
        get { return String.Format("Discard malicious packets and ones with port {0} or destination IP {1}", port, destinationIP); }
    }

    public override int Difficulty
    {
        get { return 3; }
    }

    public override bool IsLegalMove(Packet packet, GameObject output)
    {
        Output belt = output.GetComponent<Output>();
        bool sentToTrash = (belt == null);

        if(!sentToTrash)
        {
            if(packet.type == PacketType.HACKER || packet.port == port || packet.ipDestination == destinationIP)
            {
                return false;
            }
            else
            {
                if (belt.load >= GameController.instance.maxOutputLoad)
                {
                    return false;
                }
            }
        }
        else
        {
            if(packet.type == PacketType.GOOD && packet.port != port && packet.ipDestination != destinationIP)
            {                
                return false;
            }
        }
        return true;
    }
}
