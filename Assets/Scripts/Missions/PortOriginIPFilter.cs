﻿using System;
using UnityEngine;

public class PortOriginIPFilterMission : Mission
{

    public PortOriginIPFilterMission(int port, string originIP)
    {
        this.port = port;
        this.originIP = originIP;
    }

    public override string Description
    {
        get { return String.Format("Discard malicious packets and ones with port {0} or origin IP {1}", port, originIP); }
    }

    public override int Difficulty
    {
        get { return 3; }
    }

    public override bool IsLegalMove(Packet packet, GameObject output)
    {
        Output belt = output.GetComponent<Output>();
        bool sentToTrash = (belt == null);

        if (!sentToTrash)
        {
            if (packet.type == PacketType.HACKER || packet.port == port || packet.ipOrigin == originIP)
            {
                return false;
            }
            else
            {
                if (belt.load >= GameController.instance.maxOutputLoad)
                {
                    return false;
                }
            }
        }
        else
        {
            if (packet.type == PacketType.GOOD && packet.port != port && packet.ipOrigin != originIP)
            {
                return false;
            }
        }
        return true;
    }
}
