﻿using System;
using UnityEngine;

public class StandardMission : Mission
{

    public StandardMission()
    {
        return;
    }

    public override string Description
    {
        get { return "Discard malicious packets\nAccept all other traffic"; }
    }

    public override int Difficulty
    {
        get { return 1; }
    }

    public override bool IsLegalMove(Packet packet, GameObject output)
    {
        Output belt = output.GetComponent<Output>();
        bool sentToTrash = (belt == null);

        // Send a green package to trash
        if (sentToTrash && packet.type == PacketType.GOOD)
        {
            return false;
        }

        // Send a red package to any belt
        else if (!sentToTrash && packet.type == PacketType.HACKER)
        {
            return false;
        }

        // Sent to an output with too big load
        else if (!sentToTrash && belt.load >= GameController.instance.maxOutputLoad)
        {
            return false;
        }

        return true;
    }
}