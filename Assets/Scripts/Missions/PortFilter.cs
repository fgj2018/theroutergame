﻿using System;
using UnityEngine;

public class PortFilterMission : Mission
{

    public PortFilterMission(int port)
    {
        this.port = port;
    }

    public override string Description
    {
        get { return String.Format("Discard malicious packets and ones with port {0}", port); }
    }

    public override int Difficulty
    {
        get { return 2; }
    }

    public override bool IsLegalMove(Packet packet, GameObject output)
    {
        Output belt = output.GetComponent<Output>();
        bool sentToTrash = (belt == null);

        // Packet check
        if (!sentToTrash && packet.port == port)
        {
            return false;
        }

        if (sentToTrash && packet.port != port && packet.type == PacketType.GOOD)
        {
            return false;
        }

        // Send a red package to any belt
        else if (!sentToTrash && packet.type == PacketType.HACKER)
        {
            return false;
        }

        // Sent to an output with too big load
        else if (!sentToTrash && belt.load >= GameController.instance.maxOutputLoad)
        {
            return false;
        }

        return true;
    }
}