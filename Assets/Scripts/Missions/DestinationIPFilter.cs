﻿using System;
using UnityEngine;

public class DestinationIPFilterMission : Mission
{

    public DestinationIPFilterMission(string destinationIP)
    {
        this.destinationIP = destinationIP;
    }

    public override string Description
    {
        get { return String.Format("Discard malicious packets\nFrom the rest, reject the ones with destination IP {0}", destinationIP); }
    }

    public override int Difficulty
    {
        get { return 2; }
    }

    public override bool IsLegalMove(Packet packet, GameObject output)
    {
        Output belt = output.GetComponent<Output>();
        bool sentToTrash = (belt == null);

        // Packet check
        if (!sentToTrash && packet.ipDestination == destinationIP)
        {
            return false;
        }

        if (sentToTrash && packet.ipOrigin != originIP && packet.type == PacketType.GOOD)
        {
            return false;
        }

        // Send a red package to any belt
        else if (!sentToTrash && packet.type == PacketType.HACKER)
        {
            return false;
        }

        // Sent to an output with too big load
        else if (!sentToTrash && belt.load >= GameController.instance.maxOutputLoad)
        {
            return false;
        }

        return true;
    }
}
