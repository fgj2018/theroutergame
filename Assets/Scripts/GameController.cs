﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    // Audio
    [Header("Audio")]
    public AudioClip[] sounds;
    public AudioSource audioSource;

    // Sprites
    [Header("Sprites")]
    public Sprite[] packetSprites;

    // Scoring
    [Header("Scoring")]
    public int score;
    public Text[] scoreTexts;
    public Text highestScoreText;

    // Input/Outputs
    [Header("Input/Outputs")]
    public GameObject inputPort;
    public GameObject input;
    public Output[] outputs;
    public int maxOutputLoad;
    public GameObject trash;

    // Get ready screen
    [Header("Get ready screen")]
    public GameObject getReadyPanel;
    public GameObject getReadyText;

    // Packets
    [Header("Packets")]
    public GameObject packetPrefab;
    public float packetSpawnTime;
    public float packetSpeed;
    private float spawnTimer;
    public List<Packet> packetQueue;
    public List<Packet> movedPackets;
    public int maxPackets;
    public int maxPacketLoad;
    public float pBadPacket;

    // Game over
    [Header("Game over")]
    public float secondsToDie;
    private float dieTimer;
    public GameObject gameOverPanel;

    // Mission
    [Header("Mission")]
    public Mission mission;
    public Text missionText;
    public GameObject missionChange;

    // Packet inspector
    [Header("Packet inspector")]
    public Text portText;
    public Text originText;
    public Text destinationText;
    public Text sizeText;

    // DDOS
    [Header("DDOS")]
    public bool isDdos;
    public float ddosLength;
    public int minDdosTime;
    public int maxDdosTime;
    public GameObject ddosWarning;
    private float _startSpeed;
    private float _startSpawn;
    private float _nextDdosTime;

    [Header("Mission Change")]
    private int packetCounter;
    private int currentDifficulty;
    private int timesInCurrentDifficulty;

    const int PACKETS_TO_SWITCH = 20;

    public static GameController instance;


    public void StartDdos()
    {
        _nextDdosTime = Time.time + Random.Range(minDdosTime, maxDdosTime);
        isDdos = true;
        ddosWarning.SetActive(true);
        _startSpeed = packetSpeed;
        _startSpawn = packetSpawnTime;
        packetSpeed *= 1.5f;
        packetSpawnTime *= 0.6f;

        for (int i = 0; i < packetQueue.Count; i++)
        {
            packetQueue[i].speed = packetSpeed;
        }

        StartCoroutine(StopDdos());
    }

    public IEnumerator StopDdos()
    {
        yield return new WaitForSeconds(ddosLength);
        isDdos = false;
        ddosWarning.SetActive(false);
        packetSpeed = _startSpeed;
        packetSpawnTime = _startSpawn;

        for (int i = 0; i < packetQueue.Count; i++)
        {
            packetQueue[i].speed = packetSpeed;
        }
    }

    public void SetMission(Mission mission)
    {
        if (this.mission != null)
        {
            missionChange.GetComponent<Animator>().SetTrigger("Change");
        }
        spawnTimer = -2;
        this.mission = mission;
        missionText.text = mission.Description;

        foreach (Packet packet in packetQueue)
        {
            Destroy(packet.gameObject);
        }
        packetQueue.Clear();
    }

    public void UpdateMission()
    {
        packetCounter++;
        if (packetCounter == PACKETS_TO_SWITCH)
        {
            calculateNextDifficulty();
            int n;
            switch (currentDifficulty)
            {
                case 2:
                    n = Random.Range(1, 3);
                    switch (n)
                    {
                        case 1:
                            // Filter by port and type                            
                            SetMission(new PortFilterMission(Statics.getRandomPort()));
                            break;
                        case 2:
                            // Filter by origin IP and type
                            SetMission(new OriginIPFilterMission(Statics.getRandomIP()));
                            break;
                        case 3:
                            // Filter by destination IP and type
                            SetMission(new DestinationIPFilterMission(Statics.getRandomIP()));
                            break;
                        default:
                            break;
                    }
                    break;
                case 3:
                    n = Random.Range(1, 2);
                    switch (n)
                    {
                        case 1:
                            // Filter by type, port and origin IP
                            SetMission(new PortOriginIPFilterMission(Statics.getRandomPort(), Statics.getRandomIP()));
                            break;
                        case 2:
                            // Filter by type, port and destination IP
                            SetMission(new PortDestinationIPFilterMission(Statics.getRandomPort(), Statics.getRandomIP()));
                            break;
                        default:
                            break;
                    }
                    break;
                case 4:
                    // Filter by everything
                    string originIP = Statics.getRandomIP();
                    string destinationIP = Statics.getRandomIP(originIP);
                    SetMission(new PortAllIPFilterMission(Statics.getRandomPort(), originIP, destinationIP));
                    break;
                default:
                    break;
            }
            packetCounter = 0;
        }
    }

    public void UpdateInspector()
    {        

        if (packetQueue.Count == 0)
        {
            portText.text = "";
            originText.text = "";
            destinationText.text = "";
            sizeText.text = "";
        }
        else
        {
            Packet current = packetQueue[0];

            portText.text = string.Format(
                "{0} ({1})",
                current.port,
                current.portDescription
            );
            originText.text = current.ipOrigin;
            destinationText.text = current.ipDestination;
            sizeText.text = string.Format(
                "{0}kb (Load {1})",
                current.size,
                current.sizeLevel
            );
        }
    }

    public void SpawnPacket()
    {
        if (packetQueue.Count >= maxPackets)
        {
            return;
        }

        // Instantiate a new packet game object
        GameObject prefab = GameObject.Instantiate(
            packetPrefab,
            input.transform.GetChild(0).transform.position,
            Quaternion.identity
        );

        // Setup the packet parameters
        Packet packet = prefab.AddComponent<Packet>();
        packetQueue.Add(packet);
        packet.target = input.transform.GetChild(packetQueue.Count);
        packet.speed = packetSpeed;
        packet.score = 1;
        packet.size = Random.Range(20, 20 * (maxPacketLoad - 1));
        packet.sizeLevel = packet.size / 20;
        packet.ipDestination = Statics.getRandomIP();
        packet.ipOrigin = Statics.getRandomIP(packet.ipDestination);
        packet.port = Statics.getRandomPort();
        packet.portDescription = Statics.getDescription(packet.port);

        float typeValue = Random.value;
        if(typeValue <= pBadPacket)
        {
            packet.type = PacketType.HACKER;
            packet.GetComponent<SpriteRenderer>().sprite = packetSprites[1];
        }
        else
        {
            packet.type = PacketType.GOOD;
            packet.GetComponent<SpriteRenderer>().sprite = packetSprites[0];
        }

        UpdateInspector();
    }

    public void MoveToOutput(Packet packet, GameObject output)
    {
        audioSource.PlayOneShot(sounds[Random.Range(0, sounds.Length - 1)]);

        if (mission.IsLegalMove(packet, output))
        { 
            score += packet.score;
            if (score > Statics.highestScore)
            {
                Statics.highestScore = score;
            }
        }
        else
        {
            Time.timeScale = 0;
            gameOverPanel.SetActive(true);
        }

        foreach (Text scoreText in scoreTexts)
        {
            scoreText.text = score.ToString();
        }
        highestScoreText.text = Statics.highestScore.ToString();

        Output o = output.GetComponent<Output>();
        if (o != null)
        {
            o.load += packet.sizeLevel;
            o.port.GetComponent<Animator>().SetTrigger("Flicker");
        }
        inputPort.GetComponent<Animator>().SetTrigger("Flicker");

        packetQueue.Remove(packet);
        movedPackets.Add(packet);

        // Loop through any other packets in the queue and bump their targets along
        for (int i = 0; i < packetQueue.Count; i++)
        {
            packetQueue[i].target = input.transform.GetChild(i + 1);
        }

        packet.transform.position = output.transform.GetChild(0).position;
        packet.target = output.transform.GetChild(1);

        UpdateInspector();
        UpdateMission();
    }

    public IEnumerator FadeReadyScreen()
    {
        getReadyPanel.SetActive(true);
        getReadyText.SetActive(true);

        yield return new WaitForSeconds(1.5f);
        Image image = getReadyPanel.GetComponent<Image>();
        Color imageColor = image.color;

        Text text = getReadyText.GetComponent<Text>();
        Color textColor = text.color;

        for (int i = 255; i > 0; i -= 2)
        {
            yield return new WaitForSeconds(0.001f);
            imageColor.a = i / 255f;
            image.color = imageColor;

            textColor.a = i / 255f;
            text.color = textColor;
        }
        getReadyPanel.SetActive(false);
    }

    private void Start()
    {
        packetCounter = 0;
        currentDifficulty = 1;
        timesInCurrentDifficulty = 1;
        instance = this;
        _nextDdosTime = Time.time + Random.Range(minDdosTime, maxDdosTime);
        spawnTimer = -2;
        
        SetMission(new StandardMission());
        packetQueue = new List<Packet>();
        movedPackets = new List<Packet>();
        StartCoroutine(FadeReadyScreen());
    }

    private void calculateNextDifficulty()
    {
        if(currentDifficulty == 1)
        {
            timesInCurrentDifficulty = 1;
            currentDifficulty = 2;
        }
        else if(currentDifficulty == 2)
        {
            if(timesInCurrentDifficulty == 2)
            {
                timesInCurrentDifficulty = 1;
                currentDifficulty = 3;
            }
            else
            {
                timesInCurrentDifficulty++;
            }
        }
        else if(currentDifficulty == 3)
        {
            if(timesInCurrentDifficulty == 2)
            {
                timesInCurrentDifficulty = 1;
                currentDifficulty = 4;
            }
            else
            {
                timesInCurrentDifficulty++;
            }
        }
        else
        {
            timesInCurrentDifficulty++;
        }

    }

    private void Update()
    {
        spawnTimer += Time.deltaTime;
        if (spawnTimer > packetSpawnTime)
        {
            spawnTimer = 0;
            SpawnPacket();
        }

        if (_nextDdosTime < Time.time)
        {
            StartDdos();
        }

        // Too many packets on the queue
        if (packetQueue.Count == maxPackets)
        {
            dieTimer += Time.deltaTime;
            if(dieTimer >= secondsToDie)
            {
                Time.timeScale = 0;
                gameOverPanel.SetActive(true);
            }
        }
        else
        {
            dieTimer = 0f;
        }

        for (int i = 0; i < movedPackets.Count; i++)
        {
            if (movedPackets[i].isAtTarget)
            {
                Destroy(movedPackets[i].gameObject);
                movedPackets.RemoveAt(i);
            }
        }

        // If the queue has a packet that's at it's target, let's check the inputs
        if (packetQueue.Count > 1 && packetQueue[0].isAtTarget)
        {
            if (Input.GetButtonDown("Output1"))
            {
                MoveToOutput(packetQueue[0], outputs[0].gameObject);
            }
            if (Input.GetButtonDown("Output2"))
            {
                MoveToOutput(packetQueue[0], outputs[1].gameObject);
            }
            if (Input.GetButtonDown("Output3"))
            {
                MoveToOutput(packetQueue[0], outputs[2].gameObject);
            }
            if (Input.GetButtonDown("Output4"))
            {
                MoveToOutput(packetQueue[0], outputs[3].gameObject);
            }
            if (Input.GetButtonDown("Output5"))
            {
                MoveToOutput(packetQueue[0], outputs[4].gameObject);
            }
            if (Input.GetButtonDown("Trash"))
            {
                MoveToOutput(packetQueue[0], trash);
            }
        }
    }

    public void RestartScene()
    {
        Time.timeScale = 1;
        int scene = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(scene, LoadSceneMode.Single);
    }
}
