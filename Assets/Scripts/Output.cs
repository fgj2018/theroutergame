﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Output : MonoBehaviour
{
    public float load;
    public float recoverSpeed;

    public Color emptyColor;
    public Color fullColor;

    public GameObject port;

    public float loadPercentage
    {
        get {

            if(GameController.instance == null)
            {
                return 0;
            }
            return load / (float)GameController.instance.maxOutputLoad;
        }
    }

    public void SetColor(Color color)
    {
        SpriteRenderer renderer = GetComponent<SpriteRenderer>();
        color.a = 1;
        renderer.color = color;
    }

    void Update()
    {
        SetColor(Color.Lerp(emptyColor, fullColor, loadPercentage));

        if (load > 0)
        {
            load -= recoverSpeed * Time.deltaTime;
        }
        else
        {
            load = 0;
        }
	}
}
