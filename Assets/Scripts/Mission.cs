﻿using UnityEngine;

public abstract class Mission
{

    public int port;
    public string originIP;
    public string destinationIP;

    public abstract string Description { get; }
    public abstract int Difficulty { get; }

    public abstract bool IsLegalMove(Packet packet, GameObject output);
}