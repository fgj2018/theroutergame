﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    public GameObject howTo;
    public GameObject credits;

    public void StartGame()
    {
        SceneManager.LoadScene(1);
    }

    public void ShowHowTo()
    {
        howTo.SetActive(true);
    }

    public void HideHowTo()
    {
        howTo.SetActive(false);
    }

    public void ShowCredits()
    {
        credits.SetActive(true);
    }

    public void HideCredits()
    {
        credits.SetActive(false);
    }
}
