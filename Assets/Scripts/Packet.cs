﻿using UnityEngine;

public enum PacketType
{
    GOOD, HACKER
}

public class Packet : MonoBehaviour
{
    public Transform target;
    public float speed;
    public int score;
    public PacketType type;
    public int port;
    public int size;
    public int sizeLevel; // Number of levels that increases the load of the output
    public string ipOrigin;
    public string ipDestination;
    public string portDescription;

    public bool isAtTarget
    {
        get { return Vector3.Distance(transform.position, target.position) < float.Epsilon; }
    }
    
	private void Update()
    {
        transform.position = Vector3.MoveTowards(
            transform.position,
            target.transform.position,
            speed * Time.deltaTime
        );
	}

    public void SetColor(Color color)
    {
        MeshRenderer renderer = GetComponent<MeshRenderer>();
        Material material = new Material(renderer.material);
        material.color = color;
        renderer.material = material;
    }
}
