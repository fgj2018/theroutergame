﻿

using UnityEngine;

public class Statics
{
    public static int highestScore;

    public static string[] IPs = {
        "252.2.190.155", "81.32.180.160", "67.60.24.93", "85.222.165.121",
        "10.45.130.232", "220.156.72.217", "2.24.242.146", "99.247.219.208",
        "157.236.178.182", "7.156.69.253", "63.41.252.144", "60.70.201.211",
        "78.236.174.7", "255.69.42.51", "103.18.179.48", "82.132.244.11",
        "237.30.127.227", "89.153.205.187", "130.35.46.30", "131.240.134.165",
        "85.55.78.49", "104.110.146.150", "135.21.92.234", "186.245.115.136"
    };

    public static int[] portNumbers = {
        25, 37, 70, 88, 109, 110,
        443, 80, 389, 531, 17, 20,
        43, 53, 79, 220, 530, 520,
        666, 556
    };

    public static string[] portDescriptions =
    {
        "SMTP", "Time", "Gopher", "Kerberos", "POP2", "POP3",
        "HTTPS", "HTTP", "LDAP", "AIM", "QOTD", "FTP",
        "WHOIS", "DNS", "Finger", "IMAP", "RPC", "RIP",
        "Doom", "RFS"
    };

    public static int getRandomPort()
    {
        return portNumbers[Random.Range(0, Statics.portNumbers.Length - 1)];
    }

    public static string getRandomIP(string IP = "")
    {
        string ret = "";
        do
        {
            ret = IPs[Random.Range(0, Statics.IPs.Length - 1)];
        }
        while (ret == IP);
        return ret;
    }

    public static string getRandomIP()
    {
        return IPs[Random.Range(0, Statics.IPs.Length - 1)];
    }

    public static string getDescription(int port)
    {
        for(int i = 0; i < portNumbers.Length; i++)
        {
            if(portNumbers[i] == port)
            {
                return portDescriptions[i];
            }
        }
        return "";
    }
}
